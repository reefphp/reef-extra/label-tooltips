Reef.addExtension((function() {
	
	'use strict';
	
	var Extension = function(Reef) {
		this.Reef = Reef;
	};
	
	Extension.extensionName = 'reef-extra:label-tooltips';
	
	Extension.prototype.attach = function($field) {
		$field.find('.'+CSSPRFX+'label-tooltip').tooltip();
	};
	
	return Extension;
})());
