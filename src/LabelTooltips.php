<?php

namespace ReefExtra\LabelTooltips;

use \Reef\Extension\Extension;

/**
 * Extension providing tooltips besides labels
 */
class LabelTooltips extends Extension {
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'reef-extra:label-tooltips';
	}
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function getAssets() : array {
		return [];
	}
	
	/**
	 * @inherit
	 */
	public function getJS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'form.js',
				'view' => 'form',
			]
		];
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'form.css',
				'view' => 'form',
			]
		];
	}
	
	/**
	 * @inherit
	 */
	public static function getSubscribedEvents() {
		return [
			'reef.component_configuration' => 'component_configuration',
		];
	}
	
	public function component_configuration(&$a_vars) {
		
		if(!in_array('form_label_after', $a_vars['component']->getTemplateLoader()->getMeta('view/'.$this->getExtensionCollection()->getReef()->getSetup()->getLayout()->getName().'/form.mustache')['hookNames'])) {
			return;
		}
		
		$a_vars['advanced_locale']['label_tooltip'] = [
			'title' => [
				'en_US' => 'Label tooltip',
				'nl_NL' => 'Label tooltip',
			],
		];
	}
	
}
